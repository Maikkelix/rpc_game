import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  result: string;
  computer: string;
  playOptions: string[];

  ngOnInit(): void {
    this.playOptions = ["Rock", "Paper", "Sciccors"];

  }

  constructor() { }

  check(event) {
    console.log("button" + event.target.textContent + "clicked");

    // Local variable, only visible within the block (function)
    const rand = Math.floor(Math.random() * this.playOptions.length);
    const computersChoice: string = this.playOptions[rand];

    this.computer = computersChoice;
    console.log(computersChoice);

    //Trimming will remove spaces around content
    this.getResult(event.target.textContent.trim(), computersChoice);

  }
  // Arguments will act as local variables inside the function block
  getResult(pC: string, cC: string) {

    console.log(pC + "," + cC)

    if (cC === pC) {
      console.log("Draw...");
      this.result = "It´s a draw!";
    }
    else if (cC === "Rock" && pC === "Paper" ||
      cC === "Paper" && pC === "Sciccors" ||
      cC === "Scissors" && pC === "Rock") {
      console.log("Player wins!");
      this.result = "You win!";
    } 
    else {
      console.log("Computer wins!");
      this.result = "You lose!";
    }

  }

}
